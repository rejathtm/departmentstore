package com.practise;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class DepartmentStoreApplication {
	
	private static List<DepartmentStoreItem> departmentStoreItems=new ArrayList<>();
	private double totalPrice=0;
	
	public Double getTotalPrice() {
		return totalPrice;
	}

	
	public Double getTotalTax() {
		return totalTax;
	}

	

	private double totalTax=0;
	
	public void addStoreItem(String inputString) {
		departmentStoreItems.add(new DepartmentStoreItem(inputString));
	}
	
	public void printInputDetails() {
		System.out.println("Input");
        for(DepartmentStoreItem item:departmentStoreItems){
            System.out.println(item.productInitialPriceDetails());
        }
	}
	
	public void computeBill() {
		for(DepartmentStoreItem item:departmentStoreItems){
            item.calculateTax();
            totalTax+=item.getTotalTaxOfItem();
            totalPrice+=item.getTotalPriceOfItem();
        }
		totalTax=(double)Math.round(totalTax*100)/100;
		totalPrice=(double)Math.round(totalPrice*100)/100;
	}
	public void printReciept() {
		
		
        System.out.println("\nOutput");
        for(DepartmentStoreItem item:departmentStoreItems){
            System.out.println(item.productFinalPriceDetails());
        }
        System.out.println("\nTax : "+totalTax);
        System.out.println("Total Price : "+totalPrice);
	}
	
	public static void main(String[] args) {
		SpringApplication.run(DepartmentStoreApplication.class, args);
        
        
	}

}
