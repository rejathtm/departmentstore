package com.practise;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

//import org.springframework.stereotype.Component;
//@Component
public class DepartmentStoreItem{
    private double tax=0;
    private String productName;
    private int productCount;
    private double initialPrice;
    private double finalPrice=0;

    //set of items to be excluded from sales tax
    Set<String> itemToBeExcluded = new HashSet<String>(Arrays.asList("book","chocolate bar","chocolate","medicine","food"));

    public DepartmentStoreItem(String productDetails){
        String[] detailsList = productDetails.split(" : ");
        this.productName = detailsList[0];
        this.productCount = Integer.parseInt(detailsList[1]);
        this.initialPrice = Double.parseDouble(detailsList[2]);
    }

    public void calculateTax(){
        String nameInLowerCase = productName.toLowerCase();

        if(isImported(nameInLowerCase)){
            nameInLowerCase = getTruncatedName(nameInLowerCase);           
            tax+=initialPrice*0.05;
        }
        if(isSalesTaxApplicable(nameInLowerCase))
            tax+=initialPrice*0.1;
        finalPrice=initialPrice+tax;
        
    }

    private boolean isSalesTaxApplicable(String name){    
        return (!itemToBeExcluded.contains(name));
    }
    private boolean isImported(String name){   
        return name.contains("imported");
    }

    //removing imported tag
    public String getTruncatedName(String name){
        return(name.replaceAll("imported ",""));
    }
    
    public double getFinalPrice(){
        return(finalPrice);
    }
    public String getProductName(){
        return(productName);
    }
    public double getTotalTaxOfItem(){
        return(productCount*tax);
    }
    public double getTotalPriceOfItem(){
        return(finalPrice*productCount);
    }

    public String productInitialPriceDetails(){
        return(productName+':'+productCount+':'+initialPrice);
    }
    public String productFinalPriceDetails(){
        return(productName+':'+productCount+':'+finalPrice);
    }
    
}
