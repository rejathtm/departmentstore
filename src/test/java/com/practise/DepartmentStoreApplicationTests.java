package com.practise;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DepartmentStoreApplicationTests {

	@Test
	void oneTaxExclusiveItemUnimported() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Book : 1 : 10");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(10));
	}
	
	@Test
	void oneTaxExclusiveItemImported() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Imported Book : 1 : 10");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(10.5));
	}
	
	@Test
	void oneTaxInclusiveItemUnimported() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("CD : 1 : 10");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(11));
	}
	
	@Test
	void oneTaxInclusiveItemImported() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Imported CD : 1 : 10");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(11.5));
	}
	
	@Test
	void MultitipleQuantityOfOneItem() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Book : 2 : 100");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(200));
	}
	
	@Test
	void MultitipleQuantityOfOneTaxInclusiveItem() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Bat : 2 : 50");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(55));
	}
	
	@Test
	void MultitipleQuantityOfOneImportedItem() {
		
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Imported Flask : 2 : 50");
		
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(57.5));
	}
	
	@Test
	void MoreThanOneOnlyTaxInclusiveItems() {
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("CD : 1 : 10");
		departmentStoreApplication.addStoreItem("Calculator : 1 : 20");
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(33));
	}
	
	@Test
	void MoreThanOneTaxInclusiveAndImpotedItems() {
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("CD : 1 : 10");
		departmentStoreApplication.addStoreItem("Imported Calculator : 1 : 20");
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(34));
	}
	
	
	@Test
	void MoreThanOneAllTaxInclusiveItems() {
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("CD : 1 : 10");
		departmentStoreApplication.addStoreItem("Impoted Chocolate : 1 : 40");
		departmentStoreApplication.addStoreItem("Imported Calculator : 1 : 20");
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(76));
	}
	
	
	@Test
	void MoreThanOneAllTaxInclusiveItemsMultipleQuantity() {
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("CD : 1 : 10");
		departmentStoreApplication.addStoreItem("Impoted Chocolate : 2 : 40");
		departmentStoreApplication.addStoreItem("Imported Calculator : 1 : 20");
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(118));
	}
	
	@Test
	void AllItemsMultipleQuantity() {
		DepartmentStoreApplication departmentStoreApplication= new DepartmentStoreApplication();
		departmentStoreApplication.addStoreItem("Chocolate : 2 : 30");
		departmentStoreApplication.addStoreItem("CD : 3 : 10");
		departmentStoreApplication.addStoreItem("Impoted Chocolate : 2 : 40");
		departmentStoreApplication.addStoreItem("Imported Calculator : 1 : 20");
		departmentStoreApplication.computeBill();
		assertThat(departmentStoreApplication.getTotalPrice().equals(200));
	}

}
